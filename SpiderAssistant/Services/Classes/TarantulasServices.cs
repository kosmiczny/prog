﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Services
{
    public class TarantulasServices: ITarantulaServices
    {
        public IEnumerable<Ptaszniki> GetTarantulas()
        {
            using (var context = new TarantulasModel())
            {
                return new BindingList<Ptaszniki>(context.Ptaszniki.ToList());
            }
        }

        public Ptaszniki GetTarantula(int? index)
        {
            using (var context = new TarantulasModel())
            {
                return (index != null) ? context.Ptaszniki.OrderBy(x => x.Tarantula_ID).Skip((int)index).First() : context.Ptaszniki.OrderBy(x => x.Tarantula_ID).Skip(context.Ptaszniki.Count()-1).First();
            }
        }

        public void DeleteTarantula(Ptaszniki tarantulaToDelete)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(tarantulaToDelete);
                context.Ptaszniki.Remove(tarantulaToDelete);
                context.SaveChanges();
            }
            tarantulaToDelete = null;
        }

        public void ChangeAvatar(Ptaszniki selectedTarantula, Image avatar, IAvatarServices avatarServices)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(selectedTarantula);
                if (avatar != null) selectedTarantula.Image = avatarServices.ImageToByteArray(avatar);
                context.SaveChanges();
            }
        }

        public void ChangeMolt(Ptaszniki selectedTarantula, int numOfMolts)
        {
            if (numOfMolts >= 1 && numOfMolts <= 20)
            {
                using (var context = new TarantulasModel())
                {
                    context.Ptaszniki.Attach(selectedTarantula);
                    selectedTarantula.NumOfMolts = numOfMolts;
                    context.SaveChanges();
                }
            }
        }

        public void AddMolt(Ptaszniki selectedTarantula)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(selectedTarantula);
                selectedTarantula.NumOfMolts++;
                context.SaveChanges();
            }
        }

        public void FeedTarantula(Ptaszniki selectedTarantula)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(selectedTarantula);
                selectedTarantula.LastFed = DateTime.Now;
                context.SaveChanges();
            }
        }

        public void WaterTarantula(Ptaszniki selectedTarantula)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(selectedTarantula);
                selectedTarantula.LastWater = DateTime.Now;
                context.SaveChanges();
            }
        }

        public void ChangeSex(Ptaszniki selectedTarantula, string sex)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(selectedTarantula);
                selectedTarantula.Sex = sex;
                context.SaveChanges();
            }
        }

        public void ChangeLength(Ptaszniki selectedTarantula, double length)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Attach(selectedTarantula);
                selectedTarantula.Length = length;
                context.SaveChanges();
            }
        }
    }
}
