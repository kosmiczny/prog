﻿using Model;
using System.Drawing;
using System.Linq;

namespace Services
{
    public class AddTarantulaServices: IAddTarantulaServices
    {
        public Ptaszniki newTarantula;
        public Ptaszniki CreateTarantula(int id, string genusName, string specieName, string sex, double length, int? numOfMolts, Image avatar)
        {
            return newTarantula = new Ptaszniki()
            {
                Tarantula_ID = id,
                GenusName = genusName,
                SpecieName = specieName,
                Sex = sex,
                Length = length,
                NumOfMolts = numOfMolts,
                Image = avatar != null ? new AvatarServices().ImageToByteArray(avatar) : null,
                LastFed = null,
                LastWater = null
            };
        }

        public void AddTarantulaToDb(string genusName, string specieName, string sex, double length, int? numOfMolts, Image avatar)
        {
            using (var context = new TarantulasModel())
            {
                context.Ptaszniki.Add(CreateTarantula(context.Ptaszniki.Count() > 0 ? context.Ptaszniki.Select(t => t.Tarantula_ID).Max()+1 : 0,genusName, specieName, sex, length, numOfMolts, avatar));
                context.SaveChanges();
            }
        }
    }
}
