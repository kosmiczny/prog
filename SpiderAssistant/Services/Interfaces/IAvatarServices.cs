﻿using Model;
using System.Drawing;

namespace Services
{
    public interface IAvatarServices
    {
        byte[] ImageToByteArray(Image imageIn);
        Image byteArrayToImage(byte[] byteArrayIn);
    }
}
