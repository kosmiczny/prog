﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface ITarantulaServices
    {
        IEnumerable<Ptaszniki> GetTarantulas();
        Ptaszniki GetTarantula(int? index);
        void DeleteTarantula(Ptaszniki tarantulaToDelete);
        void ChangeAvatar(Ptaszniki selectedTarantula, Image avatar, IAvatarServices avatarServices);
        void ChangeMolt(Ptaszniki selectedTarantula, int numOfMolt);
        void AddMolt(Ptaszniki selectedTarantula);
        void FeedTarantula(Ptaszniki selectedTarantula);
        void WaterTarantula(Ptaszniki selectedTarantula);
        void ChangeSex(Ptaszniki selectedTarantula, string sex);
        void ChangeLength(Ptaszniki selectedTarantula, double length);
    }
}
