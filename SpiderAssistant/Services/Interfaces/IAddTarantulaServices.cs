﻿using Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IAddTarantulaServices
    {
        Ptaszniki CreateTarantula(int id, string genusName, string specieName, string sex, double length, int? numOfMolts, Image avatar);
        void AddTarantulaToDb(string genusName, string specieName, string sex, double length, int? numOfMolts, Image avatar);
    }
}
