namespace Model
{
    using System.Data.Entity;

    public partial class TarantulasModel : DbContext
    {
        public TarantulasModel()
            : base("name=TarantulasModel")
        {
        }

        public virtual DbSet<Ptaszniki> Ptaszniki { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
