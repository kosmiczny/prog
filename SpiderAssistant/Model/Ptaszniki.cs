namespace Model
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Ptaszniki")]
    public partial class Ptaszniki : INotifyPropertyChanged
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Tarantula_ID { get; set; }

        [Required]
        [StringLength(50)]
        private string _genusName;
        public string GenusName
        {
            get { return _genusName; }
            set { _genusName = value; }
        }

        [Required]
        [StringLength(50)]
        private string _specieName;
        public string SpecieName
        {
            get { return _specieName; }
            set { _specieName = value; }
        }

        [Required]
        [StringLength(50)]
        private string _sex;
        public string Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }

        private int? _numOfMolts;
        public int? NumOfMolts
        {
            get { return _numOfMolts; }
            set { _numOfMolts = value; }
        }

        private double _length;
        public double Length
        {
            get { return _length; }
            set { _length = value; }
        }

        [Column(TypeName = "image")]
        private byte[] _image;
        public byte[] Image
        {
            get { return _image; }
            set { _image = value; }
        }

        private DateTime? _lastFed;
        public DateTime? LastFed
        {
            get { return _lastFed; }
            set
            {
                _lastFed = value;
                OnPropertyChanged("LastFed");
            }
        }

        private DateTime? _lastWater;
        public DateTime? LastWater
        {
            get { return _lastWater; }
            set
            {
                _lastWater = value;
                OnPropertyChanged("LastWater");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return $"{GenusName} {SpecieName}";
        }
    }
}
