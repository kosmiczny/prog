﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class TarantulasViewModel : INotifyPropertyChanged
    {
        private BindingList<Ptaszniki> _tarantulas;
        public BindingList<Ptaszniki> tarantulas
        {
            get { return _tarantulas; }
            set
            {
                _tarantulas = value;
                OnPropertyChanged("tarantulas");
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
