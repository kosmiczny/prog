﻿using System;
using System.Linq;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using Services;
using System.Windows.Forms;

namespace SpiderAssistant
{
    public partial class AddTarantulaForm : Form
    {
        private IAddTarantulaServices _addTarantulaServices;
        public AddTarantulaForm(IAddTarantulaServices addTarantulaServices)
        {
            InitializeComponent();
            _addTarantulaServices = addTarantulaServices;
            GenusComboBox.Items.AddRange(File.ReadAllLines("Genus.data"));
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddTarantula();
            DialogResult = DialogResult.OK;
        }

        private void SetAvatarButton_Click(object sender, EventArgs e)
        {
            SetAvatar();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ClearForm();
            DialogResult = DialogResult.Cancel;
        }

        private void AddTarantula()
        {
            _addTarantulaServices.AddTarantulaToDb(
                    GenusComboBox.SelectedItem.ToString(),
                    SpeciesComboBox.SelectedItem.ToString(),
                    SexComboBox.SelectedItem.ToString(),
                    (double)LengthNumeric.Value,
                    (int?)MoltsNumeric.Value,
                    Avatar?.Image
                );
        }

        private void SetAvatar()
        {
            if (OpenAvatarFileDialog.ShowDialog() != DialogResult.Cancel)
                Avatar.ImageLocation = OpenAvatarFileDialog.FileName;
        }

        private void ClearForm()
        {
            Controls.Clear();
            InitializeComponent();
        }

        private void GenusComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetSpecies();
        }

        private void GetSpecies()
        {
            var species = File.ReadAllLines("Species.data");
            SpeciesComboBox.Items.Clear();
            SpeciesComboBox.Items.AddRange(species.Where(specie => int.Parse(specie.Split(' ')[0]) - 1 == GenusComboBox.SelectedIndex).Select(s => s.Split(' ')[1]).ToArray());
        }
    }
}
