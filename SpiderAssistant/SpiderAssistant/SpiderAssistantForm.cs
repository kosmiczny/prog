﻿using Model;
using Services;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SpiderAssistant
{
    public partial class SpiderAssistantForm : Form
    {
        private bool mouseDown;
        private Point lastLocation;
        private ITarantulaServices _tarantulaServices;
        private IAddTarantulaServices _addTarantulaServices;
        private IAvatarServices _avatarServices;
        private BindingList<Ptaszniki> _tarantulas;
        private Ptaszniki _selectedTarantula = new Ptaszniki();
        public Ptaszniki SelectedTarantula
        {
            get { return _selectedTarantula; }
            set
            {
                _selectedTarantula = value;
                OnPropertyChanged();
            }
        }

        public SpiderAssistantForm(ITarantulaServices tarantulaServices, IAddTarantulaServices addTarantulaServices, IAvatarServices avatarServices)
        {
            InitializeComponent();
            _tarantulaServices = tarantulaServices;
            _addTarantulaServices = addTarantulaServices;
            _avatarServices = avatarServices;
            _tarantulas = new BindingList<Ptaszniki>(_tarantulaServices.GetTarantulas().ToList());
            SelectTarantula.DataSource = _tarantulas;
            SelectTarantula.DropDownStyle = ComboBoxStyle.DropDownList;
            SexValueComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void addNewTarantulaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (new AddTarantulaForm(_addTarantulaServices).ShowDialog() == DialogResult.OK)
                _tarantulas.Add(new TarantulasServices().GetTarantula(null));
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            ChooseTarantula();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            _tarantulas.Remove(SelectedTarantula);
            _tarantulaServices.DeleteTarantula(SelectedTarantula);
            DisableButtons();
            ClearForm();
        }

        private void ChooseTarantula()
        {
            if (_tarantulas.Count > 0)
            {
                SelectedTarantula = _tarantulas[SelectTarantula.SelectedIndex];
                LastFedDate.DataBindings.Clear();
                LastWaterDate.DataBindings.Clear();
                LastFedDate.DataBindings.Add("Text", _selectedTarantula, "LastFed", true, DataSourceUpdateMode.OnPropertyChanged);
                LastWaterDate.DataBindings.Add("Text", _selectedTarantula, "LastWater", true, DataSourceUpdateMode.OnPropertyChanged);
                EnableButtons();
            }
        }

        private void ChangeAvatarButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.Cancel)
                Avatar.ImageLocation = openFileDialog1.FileName;
            ChangeAvatarConfirm(Avatar.Image);
        }

        private void ChangeAvatarConfirm(Image image)
        {
            DialogResult result = MessageBox.Show("Are you sure?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                _tarantulaServices.ChangeAvatar(SelectedTarantula, Avatar.Image, _avatarServices);
            }
            else
            {
                Avatar.Image = image;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void ClearForm()
        {
            Controls.Clear();
            InitializeComponent();
            SelectTarantula.DataSource = _tarantulas;
            SelectTarantula.DropDownStyle = ComboBoxStyle.DropDownList;
            SexValueComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ChangeMoltButton_Click(object sender, EventArgs e)
        {
            _tarantulaServices.ChangeMolt(SelectedTarantula, int.Parse(MoltValue.Text));
            MoltLabel.Text = "Age:";
        }

        private void AddMoltButton_Click(object sender, EventArgs e)
        {
            _tarantulaServices.AddMolt(SelectedTarantula);
            MoltValue.Text = (int.Parse(MoltValue.Text) + 1).ToString();
        }

        private void MoltValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == 8))
            {
                e.Handled = true;
            }
            else
            {
                MoltLabel.Text = "Age:*";
            }
        }

        private void Avatar_Click(object sender, EventArgs e)
        {
            ZoomAvatar();
        }

        private void ZoomAvatar()
        {
            var AvatarZoom = Avatar.Image != null ? new Avatar(Avatar.Image) : null;
            if (AvatarZoom != null)
            {
                AvatarZoom.ShowDialog();
            }
        }

        protected virtual void OnPropertyChanged()
        {
            NameValueLabel.Text = SelectedTarantula != null ? $"{SelectedTarantula.GenusName} {SelectedTarantula.SpecieName}" : "- - -";
            SexValueComboBox.Text = SelectedTarantula != null ? $"{SelectedTarantula.Sex}" : "- - -";
            LengthValue.Value = SelectedTarantula != null ? decimal.Parse(SelectedTarantula.Length.ToString()) : decimal.Parse("0.0");
            MoltValue.Text = SelectedTarantula != null ? $"{SelectedTarantula.NumOfMolts}" : string.Empty;
            byte[] avatar = SelectedTarantula != null ? SelectedTarantula.Image : null;
            Avatar.Image = avatar != null ? _avatarServices.byteArrayToImage(avatar) : null;
            LastFedDate.Text = SelectedTarantula != null ? $"{SelectedTarantula.LastFed}" : "never";
            LastFedHoursAgo.Text = SelectedTarantula != null ? SelectedTarantula.LastFed.HoursAgo() : string.Empty;
            LastWaterDate.Text = SelectedTarantula != null ? $"{SelectedTarantula.LastWater}" : "never";
            LastWaterHoursAgo.Text = SelectedTarantula != null ? SelectedTarantula.LastWater.HoursAgo() : string.Empty;
        }

        private void EnableButtons()
        {
            DeleteButton.Enabled = true;
            ChangeAvatarButton.Enabled = true;
            SaveMoltButton.Enabled = true;
            AddMoltButton.Enabled = true;
            MoltValue.Enabled = true;
            FeedButton.Enabled = true;
            WaterButton.Enabled = true;
            SexValueComboBox.Enabled = true;
            LengthValue.Enabled = true;
            ChangeSexButton.Enabled = true;
            ChangeLengthButton.Enabled = true;
        }

        private void DisableButtons()
        {
            DeleteButton.Enabled = false;
            ChangeAvatarButton.Enabled = false;
            SaveMoltButton.Enabled = false;
            AddMoltButton.Enabled = false;
            MoltValue.Enabled = false;
            SexValueComboBox.Enabled = false;
            LengthValue.Enabled = false;
            ChangeSexButton.Enabled = false;
            ChangeLengthButton.Enabled = false;
        }

        private void FeedButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Last fed date time will be set to: {DateTime.Now}", "Feeding", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                _tarantulaServices.FeedTarantula(SelectedTarantula);
                LastFedHoursAgo.Text = SelectedTarantula.LastFed.HoursAgo();
            }
        }

        private void WaterButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Last water date time will be set to: {DateTime.Now}", "Watering", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                _tarantulaServices.WaterTarantula(SelectedTarantula);
                LastWaterHoursAgo.Text = SelectedTarantula.LastWater.HoursAgo();
            }
        }

        private void ChangeSexButton_Click(object sender, EventArgs e)
        {

            SexLabel.Text = "Sex:";
        }

        private void SexValueComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            _tarantulaServices.ChangeSex(SelectedTarantula, SexValueComboBox.SelectedItem.ToString());
            SexLabel.Text = "Sex:*";
        }

        private void ChangeLengthButton_Click(object sender, EventArgs e)
        {
            _tarantulaServices.ChangeLength(SelectedTarantula, (double)LengthValue.Value);
        }

        private void SpiderAssistantForm_Load(object sender, EventArgs e)
        {

        }

        private void ExitButton_MouseEnter(object sender, EventArgs e)
        {
            ExitButton.ImageLocation = @"..\..\images\exitLightButton.png";
        }

        private void ExitButton_MouseLeave(object sender, EventArgs e)
        {
            ExitButton.ImageLocation =  @"..\..\images\exitButton.png";
        }

        private void MaximizeButton_Click(object sender, EventArgs e)
        {
            MaximizeButton.ImageLocation = WindowState == FormWindowState.Normal ? @"..\..\images\unmaximizeButton.png" : @"..\..\images\maximizeButton.png";
            WindowState = WindowState == FormWindowState.Normal ? FormWindowState.Maximized : FormWindowState.Normal;
        }

        private void MaximizeButton_MouseEnter(object sender, EventArgs e)
        {
            MaximizeButton.ImageLocation = WindowState == FormWindowState.Normal ? @"..\..\images\maximizeLightButton.png" : @"..\..\images\unmaximizeLightButton.png";
        }

        private void MaximizeButton_MouseLeave(object sender, EventArgs e)
        {
            MaximizeButton.ImageLocation = WindowState == FormWindowState.Normal ? @"..\..\images\maximizeButton.png" : @"..\..\images\unmaximizeButton.png";
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MinimizeButton_MouseEnter(object sender, EventArgs e)
        {
            MinimizeButton.ImageLocation = @"..\..\images\minimizeLightButton.png";
        }

        private void MinimizeButton_MouseLeave(object sender, EventArgs e)
        {
            MinimizeButton.ImageLocation = @"..\..\images\minimizeButton.png";
        }

        private void Title_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void Title_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                WindowState = FormWindowState.Normal;
                MaximizeButton.ImageLocation = @"..\..\images\maximizeButton.png";
                Location = new Point(
                    (Location.X - lastLocation.X) + e.X, (Location.Y - lastLocation.Y) + e.Y);
                Update();
            }
        }

        private void Title_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
    }

    public static class Extensions
    {
        public static string HoursAgo(this DateTime? lastDate)
        {
            if (lastDate != null)
                return $"({(int)(DateTime.Now - lastDate).Value.TotalHours} hours ago)";
            else return "never";
        }
    }
}
