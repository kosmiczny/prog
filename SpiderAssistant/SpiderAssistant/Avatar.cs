﻿using Services;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SpiderAssistant
{
    public partial class Avatar : Form
    {
        public Avatar(Image avatar)
        {
            InitializeComponent();
            SetAvatar(avatar);
            SetSize();
        }

        private void AvatarZoom_DoubleClick(object sender, EventArgs e)
        {
            Close();
        }

        private void SetAvatar(Image avatar)
        {
            AvatarZoom.Image = avatar;
        }

        private void SetSize()
        {
            Width = AvatarZoom.Image.Width;
            Height = AvatarZoom.Image.Height;
        }
    }
}
