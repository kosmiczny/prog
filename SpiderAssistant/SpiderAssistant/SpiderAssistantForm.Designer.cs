﻿namespace SpiderAssistant
{
    partial class SpiderAssistantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpiderAssistantForm));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.DeleteButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.SelectTarantula = new System.Windows.Forms.ComboBox();
            this.SaveMoltButton = new System.Windows.Forms.Button();
            this.AddMoltButton = new System.Windows.Forms.Button();
            this.WaterButton = new System.Windows.Forms.Button();
            this.FeedButton = new System.Windows.Forms.Button();
            this.Avatar = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.ptasznikiTableAdapter1 = new SpiderAssistant.SpiderAssistantDataSetTableAdapters.PtasznikiTableAdapter();
            this.TitleBar = new System.Windows.Forms.Panel();
            this.Title = new System.Windows.Forms.TableLayoutPanel();
            this.ExitButton = new System.Windows.Forms.PictureBox();
            this.MaximizeButton = new System.Windows.Forms.PictureBox();
            this.MinimizeButton = new System.Windows.Forms.PictureBox();
            this.Icon = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.logInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logInToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddTarantulaMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewHelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.TechnicalSupportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.AboutAuthorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.GeneralInfgroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameValueLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.LengthLabel = new System.Windows.Forms.Label();
            this.ChangeLengthButton = new System.Windows.Forms.Button();
            this.LengthValue = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SexLabel = new System.Windows.Forms.Label();
            this.ChangeSexButton = new System.Windows.Forms.Button();
            this.SexValueComboBox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.ChangeAvatarButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.L = new System.Windows.Forms.TextBox();
            this.MoltValue = new System.Windows.Forms.TextBox();
            this.MoltLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.FunctionsgroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.LastFedLabel = new System.Windows.Forms.Label();
            this.LastFedDate = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.LastWaterDate = new System.Windows.Forms.Label();
            this.LastWaterLabel = new System.Windows.Forms.Label();
            this.LastWaterHoursAgo = new System.Windows.Forms.Label();
            this.LastFedHoursAgo = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).BeginInit();
            this.TitleBar.SuspendLayout();
            this.Title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximizeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Icon)).BeginInit();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.GeneralInfgroupBox.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LengthValue)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.FunctionsgroupBox.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // DeleteButton
            // 
            this.DeleteButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DeleteButton.BackColor = System.Drawing.Color.SkyBlue;
            this.DeleteButton.Enabled = false;
            this.DeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteButton.Location = new System.Drawing.Point(578, 3);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(74, 32);
            this.DeleteButton.TabIndex = 7;
            this.DeleteButton.Text = "Delete";
            this.toolTip1.SetToolTip(this.DeleteButton, "Click to delete tarantula. It isn\'t reversible!");
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LoadButton.BackColor = System.Drawing.Color.SkyBlue;
            this.LoadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadButton.Location = new System.Drawing.Point(498, 3);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(74, 32);
            this.LoadButton.TabIndex = 6;
            this.LoadButton.Text = "Load";
            this.toolTip1.SetToolTip(this.LoadButton, "Click to load tarantula");
            this.LoadButton.UseVisualStyleBackColor = false;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // SelectTarantula
            // 
            this.SelectTarantula.BackColor = System.Drawing.SystemColors.Window;
            this.SelectTarantula.Dock = System.Windows.Forms.DockStyle.Top;
            this.SelectTarantula.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SelectTarantula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SelectTarantula.ForeColor = System.Drawing.SystemColors.InfoText;
            this.SelectTarantula.FormattingEnabled = true;
            this.SelectTarantula.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SelectTarantula.Location = new System.Drawing.Point(3, 3);
            this.SelectTarantula.MinimumSize = new System.Drawing.Size(20, 0);
            this.SelectTarantula.Name = "SelectTarantula";
            this.SelectTarantula.Size = new System.Drawing.Size(489, 32);
            this.SelectTarantula.TabIndex = 0;
            this.toolTip1.SetToolTip(this.SelectTarantula, "Choose Tarantula");
            // 
            // SaveMoltButton
            // 
            this.SaveMoltButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SaveMoltButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.SaveMoltButton.Enabled = false;
            this.SaveMoltButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveMoltButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SaveMoltButton.Location = new System.Drawing.Point(64, 3);
            this.SaveMoltButton.Name = "SaveMoltButton";
            this.SaveMoltButton.Size = new System.Drawing.Size(48, 22);
            this.SaveMoltButton.TabIndex = 2;
            this.SaveMoltButton.Text = "Save";
            this.toolTip1.SetToolTip(this.SaveMoltButton, "Click to save molt changes");
            this.SaveMoltButton.UseVisualStyleBackColor = false;
            this.SaveMoltButton.Click += new System.EventHandler(this.ChangeMoltButton_Click);
            // 
            // AddMoltButton
            // 
            this.AddMoltButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.AddMoltButton.Enabled = false;
            this.AddMoltButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddMoltButton.Location = new System.Drawing.Point(3, 73);
            this.AddMoltButton.Name = "AddMoltButton";
            this.AddMoltButton.Size = new System.Drawing.Size(120, 25);
            this.AddMoltButton.TabIndex = 15;
            this.AddMoltButton.Text = "Molt";
            this.toolTip1.SetToolTip(this.AddMoltButton, "Click to add molt");
            this.AddMoltButton.UseVisualStyleBackColor = false;
            this.AddMoltButton.Click += new System.EventHandler(this.AddMoltButton_Click);
            // 
            // WaterButton
            // 
            this.WaterButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.WaterButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.WaterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.WaterButton.Enabled = false;
            this.WaterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WaterButton.Location = new System.Drawing.Point(19, 105);
            this.WaterButton.Name = "WaterButton";
            this.WaterButton.Size = new System.Drawing.Size(200, 33);
            this.WaterButton.TabIndex = 6;
            this.WaterButton.Text = "Water";
            this.toolTip1.SetToolTip(this.WaterButton, "Set new water date\r\n");
            this.WaterButton.UseVisualStyleBackColor = false;
            this.WaterButton.Click += new System.EventHandler(this.WaterButton_Click);
            // 
            // FeedButton
            // 
            this.FeedButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FeedButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FeedButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.FeedButton.Enabled = false;
            this.FeedButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FeedButton.Location = new System.Drawing.Point(19, 35);
            this.FeedButton.Name = "FeedButton";
            this.FeedButton.Size = new System.Drawing.Size(200, 31);
            this.FeedButton.TabIndex = 4;
            this.FeedButton.Text = "Feed";
            this.toolTip1.SetToolTip(this.FeedButton, "Set new feed date\r\n");
            this.FeedButton.UseVisualStyleBackColor = false;
            this.FeedButton.Click += new System.EventHandler(this.FeedButton_Click);
            // 
            // Avatar
            // 
            this.Avatar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Avatar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Avatar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Avatar.ImageLocation = "";
            this.Avatar.Location = new System.Drawing.Point(16, 3);
            this.Avatar.Name = "Avatar";
            this.Avatar.Size = new System.Drawing.Size(100, 100);
            this.Avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Avatar.TabIndex = 1;
            this.Avatar.TabStop = false;
            this.toolTip1.SetToolTip(this.Avatar, "Click to zoom avatar");
            this.Avatar.Click += new System.EventHandler(this.Avatar_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "PNG|*.png|JPG|*.jpg|BMP|*.bmp|GIF|*.gif";
            // 
            // ptasznikiTableAdapter1
            // 
            this.ptasznikiTableAdapter1.ClearBeforeFill = true;
            // 
            // TitleBar
            // 
            this.TitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(191)))), ((int)(((byte)(240)))));
            this.TitleBar.Controls.Add(this.Title);
            this.TitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TitleBar.Location = new System.Drawing.Point(5, 0);
            this.TitleBar.Margin = new System.Windows.Forms.Padding(0);
            this.TitleBar.Name = "TitleBar";
            this.TitleBar.Size = new System.Drawing.Size(655, 26);
            this.TitleBar.TabIndex = 16;
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(191)))), ((int)(((byte)(240)))));
            this.Title.ColumnCount = 5;
            this.Title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.Title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.Title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.Title.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Title.Controls.Add(this.ExitButton, 4, 0);
            this.Title.Controls.Add(this.MaximizeButton, 3, 0);
            this.Title.Controls.Add(this.MinimizeButton, 2, 0);
            this.Title.Controls.Add(this.Icon, 0, 0);
            this.Title.Controls.Add(this.panel1, 1, 0);
            this.Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.Title.Location = new System.Drawing.Point(0, 0);
            this.Title.Name = "Title";
            this.Title.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.Title.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Title.RowCount = 1;
            this.Title.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Title.Size = new System.Drawing.Size(655, 23);
            this.Title.TabIndex = 0;
            this.Title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Title_MouseDown);
            this.Title.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Title_MouseMove);
            this.Title.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Title_MouseUp);
            // 
            // ExitButton
            // 
            this.ExitButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ExitButton.Image = ((System.Drawing.Image)(resources.GetObject("ExitButton.Image")));
            this.ExitButton.Location = new System.Drawing.Point(607, 0);
            this.ExitButton.Margin = new System.Windows.Forms.Padding(0);
            this.ExitButton.MaximumSize = new System.Drawing.Size(45, 20);
            this.ExitButton.MinimumSize = new System.Drawing.Size(45, 20);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(45, 20);
            this.ExitButton.TabIndex = 0;
            this.ExitButton.TabStop = false;
            this.ExitButton.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            this.ExitButton.MouseEnter += new System.EventHandler(this.ExitButton_MouseEnter);
            this.ExitButton.MouseLeave += new System.EventHandler(this.ExitButton_MouseLeave);
            // 
            // MaximizeButton
            // 
            this.MaximizeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.MaximizeButton.Image = global::SpiderAssistant.Properties.Resources.maximizeButton1;
            this.MaximizeButton.Location = new System.Drawing.Point(580, 0);
            this.MaximizeButton.Margin = new System.Windows.Forms.Padding(0);
            this.MaximizeButton.MaximumSize = new System.Drawing.Size(25, 20);
            this.MaximizeButton.MinimumSize = new System.Drawing.Size(25, 20);
            this.MaximizeButton.Name = "MaximizeButton";
            this.MaximizeButton.Size = new System.Drawing.Size(25, 20);
            this.MaximizeButton.TabIndex = 1;
            this.MaximizeButton.TabStop = false;
            this.MaximizeButton.Click += new System.EventHandler(this.MaximizeButton_Click);
            this.MaximizeButton.MouseEnter += new System.EventHandler(this.MaximizeButton_MouseEnter);
            this.MaximizeButton.MouseLeave += new System.EventHandler(this.MaximizeButton_MouseLeave);
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.MinimizeButton.Image = global::SpiderAssistant.Properties.Resources.minimizeButton;
            this.MinimizeButton.Location = new System.Drawing.Point(553, 0);
            this.MinimizeButton.Margin = new System.Windows.Forms.Padding(0);
            this.MinimizeButton.MaximumSize = new System.Drawing.Size(25, 20);
            this.MinimizeButton.MinimumSize = new System.Drawing.Size(25, 20);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(25, 20);
            this.MinimizeButton.TabIndex = 2;
            this.MinimizeButton.TabStop = false;
            this.MinimizeButton.Click += new System.EventHandler(this.MinimizeButton_Click);
            this.MinimizeButton.MouseEnter += new System.EventHandler(this.MinimizeButton_MouseEnter);
            this.MinimizeButton.MouseLeave += new System.EventHandler(this.MinimizeButton_MouseLeave);
            // 
            // Icon
            // 
            this.Icon.Image = ((System.Drawing.Image)(resources.GetObject("Icon.Image")));
            this.Icon.Location = new System.Drawing.Point(0, 0);
            this.Icon.Margin = new System.Windows.Forms.Padding(0);
            this.Icon.Name = "Icon";
            this.Icon.Size = new System.Drawing.Size(20, 23);
            this.Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Icon.TabIndex = 3;
            this.Icon.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.panel1.Location = new System.Drawing.Point(20, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel1.Size = new System.Drawing.Size(533, 23);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(533, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Spider Assistant";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Title_MouseDown);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Title_MouseMove);
            this.label1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Title_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 20, 2, 2);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenu,
            this.HelpMenu});
            this.menuStrip1.Location = new System.Drawing.Point(5, 26);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(655, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // FileMenu
            // 
            this.FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logInToolStripMenuItem,
            this.AddTarantulaMenu,
            this.toolStripSeparator1,
            this.ExitMenu});
            this.FileMenu.Name = "FileMenu";
            this.FileMenu.Size = new System.Drawing.Size(37, 20);
            this.FileMenu.Text = "&File";
            // 
            // logInToolStripMenuItem
            // 
            this.logInToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.logInToolStripMenuItem1,
            this.logOutToolStripMenuItem});
            this.logInToolStripMenuItem.Name = "logInToolStripMenuItem";
            this.logInToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.logInToolStripMenuItem.Text = "Profile";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.createToolStripMenuItem.Text = "Create";
            // 
            // logInToolStripMenuItem1
            // 
            this.logInToolStripMenuItem1.Name = "logInToolStripMenuItem1";
            this.logInToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.logInToolStripMenuItem1.Text = "Log In";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Enabled = false;
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            // 
            // AddTarantulaMenu
            // 
            this.AddTarantulaMenu.Name = "AddTarantulaMenu";
            this.AddTarantulaMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.AddTarantulaMenu.Size = new System.Drawing.Size(191, 22);
            this.AddTarantulaMenu.Text = "&Add Tarantula";
            this.AddTarantulaMenu.Click += new System.EventHandler(this.addNewTarantulaToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(188, 6);
            // 
            // ExitMenu
            // 
            this.ExitMenu.Name = "ExitMenu";
            this.ExitMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.ExitMenu.Size = new System.Drawing.Size(191, 22);
            this.ExitMenu.Text = "Exit";
            this.ExitMenu.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // HelpMenu
            // 
            this.HelpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ViewHelpMenu,
            this.TechnicalSupportMenu,
            this.toolStripSeparator2,
            this.AboutAuthorMenu});
            this.HelpMenu.Name = "HelpMenu";
            this.HelpMenu.Size = new System.Drawing.Size(44, 20);
            this.HelpMenu.Text = "Help";
            // 
            // ViewHelpMenu
            // 
            this.ViewHelpMenu.Name = "ViewHelpMenu";
            this.ViewHelpMenu.Size = new System.Drawing.Size(170, 22);
            this.ViewHelpMenu.Text = "View Help";
            // 
            // TechnicalSupportMenu
            // 
            this.TechnicalSupportMenu.Name = "TechnicalSupportMenu";
            this.TechnicalSupportMenu.Size = new System.Drawing.Size(170, 22);
            this.TechnicalSupportMenu.Text = "Technical Support";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(167, 6);
            // 
            // AboutAuthorMenu
            // 
            this.AboutAuthorMenu.Name = "AboutAuthorMenu";
            this.AboutAuthorMenu.Size = new System.Drawing.Size(170, 22);
            this.AboutAuthorMenu.Text = "About Author";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.AutoScroll = true;
            this.tableLayoutPanel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel8.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel8.Controls.Add(this.DeleteButton, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.LoadButton, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.SelectTarantula, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(5, 50);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.Size = new System.Drawing.Size(655, 39);
            this.tableLayoutPanel8.TabIndex = 18;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.tableLayoutPanel9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.groupBox1.Location = new System.Drawing.Point(5, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(655, 333);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel9.Controls.Add(this.GeneralInfgroupBox, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.FunctionsgroupBox, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(649, 314);
            this.tableLayoutPanel9.TabIndex = 6;
            // 
            // GeneralInfgroupBox
            // 
            this.GeneralInfgroupBox.BackColor = System.Drawing.Color.Transparent;
            this.GeneralInfgroupBox.Controls.Add(this.tableLayoutPanel10);
            this.GeneralInfgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GeneralInfgroupBox.Location = new System.Drawing.Point(3, 3);
            this.GeneralInfgroupBox.Name = "GeneralInfgroupBox";
            this.GeneralInfgroupBox.Size = new System.Drawing.Size(393, 308);
            this.GeneralInfgroupBox.TabIndex = 2;
            this.GeneralInfgroupBox.TabStop = false;
            this.GeneralInfgroupBox.Text = "General information";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel12, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.Avatar, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.groupBox2, 1, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(387, 289);
            this.tableLayoutPanel10.TabIndex = 14;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(135, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(648, 100);
            this.tableLayoutPanel7.TabIndex = 13;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.NameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.NameValueLabel, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(642, 27);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NameLabel.Location = new System.Drawing.Point(3, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(43, 27);
            this.NameLabel.TabIndex = 2;
            this.NameLabel.Text = "Name:";
            this.NameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NameValueLabel
            // 
            this.NameValueLabel.AutoSize = true;
            this.NameValueLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.NameValueLabel.Location = new System.Drawing.Point(52, 0);
            this.NameValueLabel.Name = "NameValueLabel";
            this.NameValueLabel.Size = new System.Drawing.Size(22, 27);
            this.NameValueLabel.TabIndex = 5;
            this.NameValueLabel.Text = "- - -";
            this.NameValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 536F));
            this.tableLayoutPanel3.Controls.Add(this.LengthLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.ChangeLengthButton, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.LengthValue, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 69);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(642, 28);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // LengthLabel
            // 
            this.LengthLabel.AutoSize = true;
            this.LengthLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LengthLabel.Location = new System.Drawing.Point(3, 0);
            this.LengthLabel.Name = "LengthLabel";
            this.LengthLabel.Size = new System.Drawing.Size(50, 28);
            this.LengthLabel.TabIndex = 7;
            this.LengthLabel.Text = "Length:";
            this.LengthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChangeLengthButton
            // 
            this.ChangeLengthButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ChangeLengthButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.ChangeLengthButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ChangeLengthButton.Enabled = false;
            this.ChangeLengthButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeLengthButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ChangeLengthButton.Location = new System.Drawing.Point(109, 3);
            this.ChangeLengthButton.Name = "ChangeLengthButton";
            this.ChangeLengthButton.Size = new System.Drawing.Size(47, 22);
            this.ChangeLengthButton.TabIndex = 9;
            this.ChangeLengthButton.Text = "Change";
            this.ChangeLengthButton.UseVisualStyleBackColor = false;
            this.ChangeLengthButton.Click += new System.EventHandler(this.ChangeLengthButton_Click);
            // 
            // LengthValue
            // 
            this.LengthValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LengthValue.AutoSize = true;
            this.LengthValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.LengthValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LengthValue.DecimalPlaces = 1;
            this.LengthValue.Enabled = false;
            this.LengthValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.LengthValue.Location = new System.Drawing.Point(59, 6);
            this.LengthValue.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.LengthValue.Name = "LengthValue";
            this.LengthValue.Size = new System.Drawing.Size(44, 16);
            this.LengthValue.TabIndex = 10;
            this.LengthValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 528F));
            this.tableLayoutPanel2.Controls.Add(this.SexLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ChangeSexButton, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.SexValueComboBox, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 36);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(642, 27);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // SexLabel
            // 
            this.SexLabel.AutoSize = true;
            this.SexLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SexLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SexLabel.Location = new System.Drawing.Point(3, 0);
            this.SexLabel.Name = "SexLabel";
            this.SexLabel.Size = new System.Drawing.Size(32, 28);
            this.SexLabel.TabIndex = 4;
            this.SexLabel.Text = "Sex:";
            this.SexLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChangeSexButton
            // 
            this.ChangeSexButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ChangeSexButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.ChangeSexButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.ChangeSexButton.Enabled = false;
            this.ChangeSexButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeSexButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ChangeSexButton.Location = new System.Drawing.Point(117, 3);
            this.ChangeSexButton.Name = "ChangeSexButton";
            this.ChangeSexButton.Size = new System.Drawing.Size(47, 22);
            this.ChangeSexButton.TabIndex = 7;
            this.ChangeSexButton.Text = "Change";
            this.ChangeSexButton.UseVisualStyleBackColor = false;
            this.ChangeSexButton.Click += new System.EventHandler(this.ChangeSexButton_Click);
            // 
            // SexValueComboBox
            // 
            this.SexValueComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(247)))));
            this.SexValueComboBox.Enabled = false;
            this.SexValueComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SexValueComboBox.FormattingEnabled = true;
            this.SexValueComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Male",
            "Female"});
            this.SexValueComboBox.Location = new System.Drawing.Point(41, 3);
            this.SexValueComboBox.Name = "SexValueComboBox";
            this.SexValueComboBox.Size = new System.Drawing.Size(70, 21);
            this.SexValueComboBox.TabIndex = 8;
            this.SexValueComboBox.SelectedValueChanged += new System.EventHandler(this.SexValueComboBox_SelectedValueChanged);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.ChangeAvatarButton, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.AddMoltButton, 0, 2);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 109);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 4;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(126, 244);
            this.tableLayoutPanel12.TabIndex = 14;
            // 
            // ChangeAvatarButton
            // 
            this.ChangeAvatarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.ChangeAvatarButton.Enabled = false;
            this.ChangeAvatarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeAvatarButton.Location = new System.Drawing.Point(3, 3);
            this.ChangeAvatarButton.Name = "ChangeAvatarButton";
            this.ChangeAvatarButton.Size = new System.Drawing.Size(120, 33);
            this.ChangeAvatarButton.TabIndex = 9;
            this.ChangeAvatarButton.Text = "Change avatar";
            this.ChangeAvatarButton.UseVisualStyleBackColor = false;
            this.ChangeAvatarButton.Click += new System.EventHandler(this.ChangeAvatarButton_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.Controls.Add(this.SaveMoltButton, 3, 0);
            this.tableLayoutPanel13.Controls.Add(this.L, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.MoltValue, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.MoltLabel, 0, 0);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 42);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.Size = new System.Drawing.Size(120, 25);
            this.tableLayoutPanel13.TabIndex = 10;
            // 
            // L
            // 
            this.L.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.L.BackColor = System.Drawing.SystemColors.Control;
            this.L.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.L.Location = new System.Drawing.Point(39, 7);
            this.L.Margin = new System.Windows.Forms.Padding(0);
            this.L.Name = "L";
            this.L.ReadOnly = true;
            this.L.Size = new System.Drawing.Size(6, 13);
            this.L.TabIndex = 16;
            this.L.Text = "L";
            // 
            // MoltValue
            // 
            this.MoltValue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.MoltValue.BackColor = System.Drawing.SystemColors.Window;
            this.MoltValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MoltValue.Enabled = false;
            this.MoltValue.Location = new System.Drawing.Point(45, 7);
            this.MoltValue.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.MoltValue.MaxLength = 2;
            this.MoltValue.Name = "MoltValue";
            this.MoltValue.Size = new System.Drawing.Size(13, 13);
            this.MoltValue.TabIndex = 17;
            this.MoltValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MoltValue_KeyPress);
            // 
            // MoltLabel
            // 
            this.MoltLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.MoltLabel.AutoSize = true;
            this.MoltLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MoltLabel.Location = new System.Drawing.Point(3, 7);
            this.MoltLabel.Name = "MoltLabel";
            this.MoltLabel.Size = new System.Drawing.Size(33, 13);
            this.MoltLabel.TabIndex = 0;
            this.MoltLabel.Text = "Age:";
            this.MoltLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel11);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(135, 109);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(648, 244);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "About genus";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 5;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(642, 225);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // FunctionsgroupBox
            // 
            this.FunctionsgroupBox.AutoSize = true;
            this.FunctionsgroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FunctionsgroupBox.BackColor = System.Drawing.Color.Transparent;
            this.FunctionsgroupBox.Controls.Add(this.tableLayoutPanel6);
            this.FunctionsgroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FunctionsgroupBox.Location = new System.Drawing.Point(402, 3);
            this.FunctionsgroupBox.Name = "FunctionsgroupBox";
            this.FunctionsgroupBox.Size = new System.Drawing.Size(244, 308);
            this.FunctionsgroupBox.TabIndex = 5;
            this.FunctionsgroupBox.TabStop = false;
            this.FunctionsgroupBox.Text = "Functions";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.WaterButton, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel5, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.LastWaterHoursAgo, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.LastFedHoursAgo, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.FeedButton, 0, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 6;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(238, 142);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.LastFedLabel, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.LastFedDate, 1, 0);
            this.tableLayoutPanel4.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(106, 13);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // LastFedLabel
            // 
            this.LastFedLabel.AutoSize = true;
            this.LastFedLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LastFedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LastFedLabel.Location = new System.Drawing.Point(3, 0);
            this.LastFedLabel.Name = "LastFedLabel";
            this.LastFedLabel.Size = new System.Drawing.Size(60, 13);
            this.LastFedLabel.TabIndex = 0;
            this.LastFedLabel.Text = "Last Fed:";
            // 
            // LastFedDate
            // 
            this.LastFedDate.AutoSize = true;
            this.LastFedDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.LastFedDate.Location = new System.Drawing.Point(69, 0);
            this.LastFedDate.Name = "LastFedDate";
            this.LastFedDate.Size = new System.Drawing.Size(34, 13);
            this.LastFedDate.TabIndex = 2;
            this.LastFedDate.Text = "never";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.LastWaterDate, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.LastWaterLabel, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 72);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(119, 13);
            this.tableLayoutPanel5.TabIndex = 13;
            // 
            // LastWaterDate
            // 
            this.LastWaterDate.AutoSize = true;
            this.LastWaterDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.LastWaterDate.Location = new System.Drawing.Point(82, 0);
            this.LastWaterDate.Name = "LastWaterDate";
            this.LastWaterDate.Size = new System.Drawing.Size(34, 13);
            this.LastWaterDate.TabIndex = 4;
            this.LastWaterDate.Text = "never";
            // 
            // LastWaterLabel
            // 
            this.LastWaterLabel.AutoSize = true;
            this.LastWaterLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.LastWaterLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LastWaterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LastWaterLabel.Location = new System.Drawing.Point(3, 0);
            this.LastWaterLabel.Name = "LastWaterLabel";
            this.LastWaterLabel.Size = new System.Drawing.Size(73, 13);
            this.LastWaterLabel.TabIndex = 2;
            this.LastWaterLabel.Text = "Last Water:";
            // 
            // LastWaterHoursAgo
            // 
            this.LastWaterHoursAgo.AutoSize = true;
            this.LastWaterHoursAgo.Dock = System.Windows.Forms.DockStyle.Top;
            this.LastWaterHoursAgo.Location = new System.Drawing.Point(3, 88);
            this.LastWaterHoursAgo.Name = "LastWaterHoursAgo";
            this.LastWaterHoursAgo.Size = new System.Drawing.Size(232, 13);
            this.LastWaterHoursAgo.TabIndex = 15;
            this.LastWaterHoursAgo.Text = "(0 hours ago)";
            // 
            // LastFedHoursAgo
            // 
            this.LastFedHoursAgo.AutoSize = true;
            this.LastFedHoursAgo.Dock = System.Windows.Forms.DockStyle.Left;
            this.LastFedHoursAgo.Location = new System.Drawing.Point(3, 19);
            this.LastFedHoursAgo.Name = "LastFedHoursAgo";
            this.LastFedHoursAgo.Size = new System.Drawing.Size(69, 13);
            this.LastFedHoursAgo.TabIndex = 14;
            this.LastFedHoursAgo.Text = "(0 hours ago)";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "Ptaszniki";
            // 
            // SpiderAssistantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(191)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(665, 427);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.TitleBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(390, 290);
            this.Name = "SpiderAssistantForm";
            this.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spider Assistant";
            this.Load += new System.EventHandler(this.SpiderAssistantForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).EndInit();
            this.TitleBar.ResumeLayout(false);
            this.Title.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExitButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximizeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Icon)).EndInit();
            this.panel1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.GeneralInfgroupBox.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LengthValue)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.FunctionsgroupBox.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        //private SpiderAssistant.SpiderAssistantDataSet spiderAssistantDataSet;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private SpiderAssistantDataSetTableAdapters.PtasznikiTableAdapter ptasznikiTableAdapter1;
        private System.Windows.Forms.Panel TitleBar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileMenu;
        private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AddTarantulaMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ExitMenu;
        private System.Windows.Forms.ToolStripMenuItem HelpMenu;
        private System.Windows.Forms.ToolStripMenuItem ViewHelpMenu;
        private System.Windows.Forms.ToolStripMenuItem TechnicalSupportMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem AboutAuthorMenu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.ComboBox SelectTarantula;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.GroupBox GeneralInfgroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label NameValueLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label LengthLabel;
        private System.Windows.Forms.Button ChangeLengthButton;
        private System.Windows.Forms.NumericUpDown LengthValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label SexLabel;
        private System.Windows.Forms.Button ChangeSexButton;
        private System.Windows.Forms.ComboBox SexValueComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Button ChangeAvatarButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button SaveMoltButton;
        private System.Windows.Forms.TextBox L;
        private System.Windows.Forms.TextBox MoltValue;
        private System.Windows.Forms.Label MoltLabel;
        private System.Windows.Forms.Button AddMoltButton;
        private System.Windows.Forms.PictureBox Avatar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.GroupBox FunctionsgroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button WaterButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label LastFedLabel;
        private System.Windows.Forms.Label LastFedDate;
        private System.Windows.Forms.Button FeedButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label LastWaterDate;
        private System.Windows.Forms.Label LastWaterLabel;
        private System.Windows.Forms.Label LastWaterHoursAgo;
        private System.Windows.Forms.Label LastFedHoursAgo;
        private System.Windows.Forms.TableLayoutPanel Title;
        private System.Windows.Forms.PictureBox ExitButton;
        private System.Windows.Forms.PictureBox MaximizeButton;
        private System.Windows.Forms.PictureBox MinimizeButton;
        private System.Windows.Forms.PictureBox Icon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        //private SpiderAssistant.SpiderAssistantDataSetTableAdapters.PtasznikiTableAdapter ptasznikiTableAdapter;
    }
}

