﻿namespace SpiderAssistant
{
    partial class Avatar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AvatarZoom = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AvatarZoom)).BeginInit();
            this.SuspendLayout();
            // 
            // AvatarZoom
            // 
            this.AvatarZoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AvatarZoom.Location = new System.Drawing.Point(0, 0);
            this.AvatarZoom.Name = "AvatarZoom";
            this.AvatarZoom.Size = new System.Drawing.Size(216, 220);
            this.AvatarZoom.TabIndex = 0;
            this.AvatarZoom.TabStop = false;
            this.toolTip1.SetToolTip(this.AvatarZoom, "Double click to close avatar zoom\r\n");
            this.AvatarZoom.DoubleClick += new System.EventHandler(this.AvatarZoom_DoubleClick);
            // 
            // Avatar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(216, 220);
            this.Controls.Add(this.AvatarZoom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(50, 50);
            this.Name = "Avatar";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Avatar";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.AvatarZoom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox AvatarZoom;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}