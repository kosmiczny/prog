﻿namespace SpiderAssistant
{
    partial class AddTarantulaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GenusComboBox = new System.Windows.Forms.ComboBox();
            this.SpeciesComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Avatar = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SexComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SetAvatarButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.LengthNumeric = new System.Windows.Forms.NumericUpDown();
            this.OpenAvatarFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.MoltsNumeric = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoltsNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // GenusComboBox
            // 
            this.GenusComboBox.FormattingEnabled = true;
            this.GenusComboBox.Location = new System.Drawing.Point(176, 12);
            this.GenusComboBox.Name = "GenusComboBox";
            this.GenusComboBox.Size = new System.Drawing.Size(145, 21);
            this.GenusComboBox.TabIndex = 0;
            this.GenusComboBox.SelectedIndexChanged += new System.EventHandler(this.GenusComboBox_SelectedIndexChanged);
            // 
            // SpeciesComboBox
            // 
            this.SpeciesComboBox.FormattingEnabled = true;
            this.SpeciesComboBox.Location = new System.Drawing.Point(176, 39);
            this.SpeciesComboBox.Name = "SpeciesComboBox";
            this.SpeciesComboBox.Size = new System.Drawing.Size(145, 21);
            this.SpeciesComboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(129, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Genus:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Species:";
            // 
            // Avatar
            // 
            this.Avatar.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Avatar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Avatar.Location = new System.Drawing.Point(12, 12);
            this.Avatar.Name = "Avatar";
            this.Avatar.Size = new System.Drawing.Size(100, 100);
            this.Avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Avatar.TabIndex = 4;
            this.Avatar.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(142, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sex:";
            // 
            // SexComboBox
            // 
            this.SexComboBox.FormattingEnabled = true;
            this.SexComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Male",
            "Female"});
            this.SexComboBox.Location = new System.Drawing.Point(176, 66);
            this.SexComboBox.Name = "SexComboBox";
            this.SexComboBox.Size = new System.Drawing.Size(70, 21);
            this.SexComboBox.TabIndex = 6;
            this.SexComboBox.Text = "Unknown";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Length:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "cm BL";
            // 
            // SetAvatarButton
            // 
            this.SetAvatarButton.Location = new System.Drawing.Point(25, 119);
            this.SetAvatarButton.Name = "SetAvatarButton";
            this.SetAvatarButton.Size = new System.Drawing.Size(75, 23);
            this.SetAvatarButton.TabIndex = 10;
            this.SetAvatarButton.Text = "Set Avatar";
            this.SetAvatarButton.UseVisualStyleBackColor = true;
            this.SetAvatarButton.Click += new System.EventHandler(this.SetAvatarButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(246, 146);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 11;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(165, 146);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 12;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // LengthNumeric
            // 
            this.LengthNumeric.DecimalPlaces = 2;
            this.LengthNumeric.Location = new System.Drawing.Point(176, 120);
            this.LengthNumeric.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.LengthNumeric.Name = "LengthNumeric";
            this.LengthNumeric.Size = new System.Drawing.Size(50, 20);
            this.LengthNumeric.TabIndex = 13;
            // 
            // OpenAvatarFileDialog
            // 
            this.OpenAvatarFileDialog.Filter = "PNG|*.png|JPG|*.jpg|JPEG|*.jpeg|GIF|*.gif|BMP|*.bmp";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(135, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Molts:";
            // 
            // MoltsNumeric
            // 
            this.MoltsNumeric.Location = new System.Drawing.Point(176, 94);
            this.MoltsNumeric.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.MoltsNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MoltsNumeric.Name = "MoltsNumeric";
            this.MoltsNumeric.Size = new System.Drawing.Size(36, 20);
            this.MoltsNumeric.TabIndex = 15;
            this.MoltsNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // AddTarantulaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 181);
            this.Controls.Add(this.MoltsNumeric);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LengthNumeric);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.SetAvatarButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SexComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Avatar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SpeciesComboBox);
            this.Controls.Add(this.GenusComboBox);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(348, 220);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(348, 220);
            this.Name = "AddTarantulaForm";
            this.ShowIcon = false;
            this.Text = "Add Tarantula";
            ((System.ComponentModel.ISupportInitialize)(this.Avatar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MoltsNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox GenusComboBox;
        private System.Windows.Forms.ComboBox SpeciesComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox Avatar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox SexComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button SetAvatarButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.NumericUpDown LengthNumeric;
        private System.Windows.Forms.OpenFileDialog OpenAvatarFileDialog;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown MoltsNumeric;
    }
}