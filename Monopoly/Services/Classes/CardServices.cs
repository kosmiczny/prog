﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.IO;
using CsvHelper;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace Services
{
    public class CardServices : ICardServices
    {
        public List<Card> GetCards()
        {
            var cardsData = File.ReadAllLines(@"..\..\data\cards.data").Select(card => card.Split(' ')).ToList();
            return cardsData.Select(card => new Card()
            {
                fieldPosX = int.Parse(card[0]),
                fieldPosY = int.Parse(card[1]),
                color = (SolidColorBrush)new BrushConverter().ConvertFromString($"{card[2]}"),
                placeName = string.Join(" ", card[3].Split('_')),
                buyPrice = int.Parse(card[4]),
                costs = card.Skip(5).Take(card.Count() - 5 - 1).Select(number => Convert.ToInt32(number)).ToArray(),
                mortgagePledge = int.Parse(card.Last())
            }).ToList();
        }
    }
}
