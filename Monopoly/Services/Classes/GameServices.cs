﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace Services
{
    public class GameServices : IGameServices
    {
        private Random _rnd = new Random();

        public int[] RollDices()
        {
            return new int[] { _rnd.Next(1, 6), _rnd.Next(1, 7) };
        }
    }
}
