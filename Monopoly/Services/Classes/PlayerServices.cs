﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.IO;
using System.Windows.Threading;

namespace Services
{
    public class PlayerServices : IPlayerServices
    {
        public List<string> blueChanceCards { get; set; } = File.ReadAllLines(@"..\..\data\blueChanceCards.data").ToList();
        public List<string> redChanceCards { get; set; } = File.ReadAllLines(@"..\..\data\redChanceCards.data").ToList();

        private PointCollection blueQuestionMarksPos = new PointCollection()
        {
            new Point(8,10),
            new Point(0,3),
            new Point(10,3)
        };
        private PointCollection redQuestionMarksPos = new PointCollection()
        {
            new Point(3,10),
            new Point(2,0),
            new Point(10,6)
        };

        public Player AddPlayer(string nick, Brush color)
        {
            return new Player()
            {
                nick = nick,
                cash = 3000,
                pawn = new Pawn()
                {
                    posX = 10,
                    posY = 10,
                    pawn = new Rectangle() { Fill = color }
                }
            };
        }

        public int[] MovePawn(int posX, int posY, int steps, ref string turnInfo)
        {
            int chanceCard = 0;
            var cash = 0;
            var queue = 0;
            Move(ref posX, ref posY, steps, ref cash, ref turnInfo);
            CheckJail(ref posX, ref posY, ref queue, ref turnInfo);
            CheckSpecialPlaces(posX, posY, ref cash);
            CheckChanceCardPlaces(ref posX, ref posY, ref cash, ref queue, ref chanceCard);
            return new int[] { posX, posY, cash, queue, chanceCard };
        }

        private void Move(ref int posX, ref int posY, int steps, ref int cash, ref string turnInfo)
        {
            for (int i = 0; i < steps; i++)
            {
                if (posY == 10 && posX != 0) posX--;
                else if (posX == 0 && posY != 0) posY--;
                else if (posY == 0 && posX != 10) posX++;
                else if (posX == 10 && posY != 10) posY++;
                if (posX == 10 && posY == 10) cash += 400; //start field
            }
            turnInfo = $"Przesunąłeś pionem o {steps} kroki/ów.";
        }

        private void CheckJail(ref int posX, ref int posY, ref int queue, ref string turnInfo)
        {
            if (posX == 10 && posY == 0)
            {
                posX = 0;
                posY = 10;
                queue = 2;
                turnInfo += $" Trafiłeś do więzienia na 2 kolejki.";
            }
        }

        private void CheckSpecialPlaces(int posX, int posY, ref int cash)
        {
            cash += posX == 6 && posY == 10 ? -400 : posX == 10 && posY == 8 ? -200 : 0;
        }

        private void CheckChanceCardPlaces(ref int posX, ref int posY, ref int cash, ref int queue, ref int chanceCard)
        {
            var chance = 0;
            var point = new Point(posX, posY);
            string[] cardData = null;
            if (blueQuestionMarksPos.Contains(point) == true)
            {
                chance = new Random().Next(1, 15);
                cardData = blueChanceCards.FirstOrDefault(card => int.Parse(card.Split(' ')[0]) == chance).Split(' ');
                chanceCard = chance;
            }
            if (redQuestionMarksPos.Contains(point) == true)
            {
                chance = new Random().Next(15, 25);
                cardData = redChanceCards.FirstOrDefault(card => int.Parse(card.Split(' ')[0]) == chance).Split(' ');
                chanceCard = chance;
            }
            if (cardData != null)
            {
                switch (cardData[2])
                {
                    case "CASH":
                        cash += int.Parse(cardData[3]);
                        break;
                    case "POSITION":
                        posX = int.Parse(cardData[3]);
                        posY = int.Parse(cardData[4]);
                        break;
                    case "JAIL":
                        posX = 0;
                        posY = 10;
                        queue = 2;
                        break;
                    case "MOVE":
                        posX = posX == 3 && posY == 10 ? 6 : posX == 2 && posY == 0 ? 0 : posX;
                        posY = posX == 10 && posY == 6 ? 3 : posX == 0 && posY == 0 ? 1 : posY;
                        break;
                }
            }
        }
    }
}
