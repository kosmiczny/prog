﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Collections.ObjectModel;

namespace Services
{
    public interface ICardServices
    {
        List<Card> GetCards();
    }
}
