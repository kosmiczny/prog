﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Windows.Media;

namespace Services
{
    public interface IPlayerServices
    {
        List<string> blueChanceCards { get; set; }
        List<string> redChanceCards { get; set; }
        Player AddPlayer(string nick, Brush color);
        int[] MovePawn(int posX, int posY, int steps, ref string turnInfo);
    }
}
