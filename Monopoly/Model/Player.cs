﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Model
{
    public class Player
    {
        private static int _id = 0;
        public int id { get; set; }
        public string nick { get; set; }
        public int cash { get; set; }
        public Pawn pawn { get; set; }
        public Player()
        {
            _id++;
            id = _id;
        }
    }

    public class Pawn
    {
        public int posX { get; set; }
        public int posY { get; set; }
        public Rectangle pawn { get; set; }
    }
}
