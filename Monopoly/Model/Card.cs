﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Model
{
    public class Card
    {
        public int fieldPosX { get; set; }
        public int fieldPosY { get; set; }
        public Brush color { get; set; }
        public string placeName { get; set; }
        public int buyPrice { get; set; }
        public Brush owner { get; set; } = Brushes.White;
        public int[] costs { get; set; }
        public int mortgagePledge { get; set; }
    }
}
