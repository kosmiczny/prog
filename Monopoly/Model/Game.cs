﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Model
{
    public class Game
    {
        public int firstDice { get; set; }
        public int secondDice { get; set; }
        public Brush turn { get; set; }
    }
}
