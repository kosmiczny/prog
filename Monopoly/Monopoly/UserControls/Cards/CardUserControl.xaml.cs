﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monopoly.UserControls.Cards
{
    /// <summary>
    /// Interaction logic for CardUserControl.xaml
    /// </summary>
    public partial class CardUserControl : UserControl
    {

        public Brush CircleColor
        {
            get { return (Brush)GetValue(CircleColorProperty); }
            set { SetValue(CircleColorProperty, value); }
        }
        public static readonly DependencyProperty CircleColorProperty =
            DependencyProperty.Register("CircleColor", typeof(Brush), typeof(CardUserControl), new PropertyMetadata(null));

        public string CardName
        {
            get { return (string)GetValue(CardNameProperty); }
            set { SetValue(CardNameProperty, value); }
        }
        public static readonly DependencyProperty CardNameProperty =
            DependencyProperty.Register("CardName", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));



        public Brush CardNameColor
        {
            get { return (Brush)GetValue(CardNameColorProperty); }
            set { SetValue(CardNameColorProperty, value); }
        }
        public static readonly DependencyProperty CardNameColorProperty =
            DependencyProperty.Register("CardNameColor", typeof(Brush), typeof(CardUserControl), new PropertyMetadata(Brushes.Black));



        public string FieldPrice
        {
            get { return (string)GetValue(FieldPriceProperty); }
            set { SetValue(FieldPriceProperty, value); }
        }
        public static readonly DependencyProperty FieldPriceProperty =
            DependencyProperty.Register("FieldPrice", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public string NoHouseCost
        {
            get { return (string)GetValue(NoHouseCostProperty); }
            set { SetValue(NoHouseCostProperty, value); }
        }
        public static readonly DependencyProperty NoHouseCostProperty =
            DependencyProperty.Register("NoHouseCost", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public string OneHouseCost
        {
            get { return (string)GetValue(OneHouseCostProperty); }
            set { SetValue(OneHouseCostProperty, value); }
        }
        public static readonly DependencyProperty OneHouseCostProperty =
            DependencyProperty.Register("OneHouseCost", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public string TwoHouseCost
        {
            get { return (string)GetValue(TwoHouseCostProperty); }
            set { SetValue(TwoHouseCostProperty, value); }
        }
        public static readonly DependencyProperty TwoHouseCostProperty =
            DependencyProperty.Register("TwoHouseCost", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public string ThreeHouseCost
        {
            get { return (string)GetValue(ThreeHouseCostProperty); }
            set { SetValue(ThreeHouseCostProperty, value); }
        }
        public static readonly DependencyProperty ThreeHouseCostProperty =
            DependencyProperty.Register("ThreeHouseCost", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public string FourHouseCost
        {
            get { return (string)GetValue(FourHouseCostProperty); }
            set { SetValue(FourHouseCostProperty, value); }
        }
        public static readonly DependencyProperty FourHouseCostProperty =
            DependencyProperty.Register("FourHouseCost", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public string HotelCost
        {
            get { return (string)GetValue(HotelCostProperty); }
            set { SetValue(HotelCostProperty, value); }
        }
        public static readonly DependencyProperty HotelCostProperty =
            DependencyProperty.Register("HotelCost", typeof(string), typeof(CardUserControl), new PropertyMetadata(string.Empty));

        public CardUserControl()
        {
            InitializeComponent();
        }
    }
}
