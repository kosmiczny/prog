﻿using System.Windows;
using System.Windows.Controls;

namespace Monopoly.UserControls.Cards
{
    /// <summary>
    /// Interaction logic for ChanceCard.xaml
    /// </summary>
    public partial class ChanceCard : UserControl
    {
        public string BonusText
        {
            get { return (string)GetValue(BonusTextProperty); }
            set { SetValue(BonusTextProperty, value); }
        }
        public static readonly DependencyProperty BonusTextProperty =
            DependencyProperty.Register("BonusText", typeof(string), typeof(ChanceCard), new PropertyMetadata(string.Empty));

        public object CardColor
        {
            get { return GetValue(CardColorProperty); }
            set { SetValue(CardColorProperty, value); }
        }
        public static readonly DependencyProperty CardColorProperty =
            DependencyProperty.Register("CardColor", typeof(object), typeof(ChanceCard), new PropertyMetadata(null));




        public ChanceCard()
        {
            InitializeComponent();
        }
    }
}
