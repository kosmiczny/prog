﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monopoly.UserControls.Players
{
    /// <summary>
    /// Interaction logic for PawnUC.xaml
    /// </summary>
    public partial class PawnUC : UserControl
    {
        public Brush PawnColor
        {
            get { return (Brush)GetValue(PawnColorProperty); }
            set { SetValue(PawnColorProperty, value); }
        }
        public static readonly DependencyProperty PawnColorProperty =
            DependencyProperty.Register("PawnColor", typeof(Brush), typeof(PawnUC), new PropertyMetadata(Brushes.Transparent));

        public PawnUC()
        {
            InitializeComponent();
        }
    }
}
