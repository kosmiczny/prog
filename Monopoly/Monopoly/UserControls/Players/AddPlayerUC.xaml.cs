﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monopoly.UserControls.Players
{
    /// <summary>
    /// Interaction logic for AddPlayerUC.xaml
    /// </summary>
    public partial class AddPlayerUC : UserControl
    {
        public static List<string> colors { get; set; } = new List<string>() { "Red", "Green", "Blue", "Purple", "Yellow", "Pink", "Cyan" };
        public static List<PropertyInfo> colorsList { get; set; } = typeof(Colors).GetProperties().Where(color => colors.Exists(c => c == color.Name)).ToList();

        private void EnableAddButton(object sender, SelectionChangedEventArgs e)
        {
            AddPlayerButton.IsEnabled = true;
        }

        private void AddPlayerButton_Click(object sender, RoutedEventArgs e)
        {
            PlayerColor = new BrushConverter().ConvertFromString(((PropertyInfo)PawnColor.SelectedItem).Name) as SolidColorBrush;
            colorsList.Remove((PropertyInfo)PawnColor.SelectedItem);
        }

        public ICommand AddPlayerCommand
        {
            get { return (ICommand)GetValue(AddPlayerCommandProperty); }
            set { SetValue(AddPlayerCommandProperty, value); }
        }
        public static readonly DependencyProperty AddPlayerCommandProperty =
            DependencyProperty.Register("AddPlayerCommand", typeof(ICommand), typeof(AddPlayerUC), new PropertyMetadata(null));

        public string PlayerNick
        {
            get { return (string)GetValue(PlayerNickProperty); }
            set { SetValue(PlayerNickProperty, value); }
        }
        public static readonly DependencyProperty PlayerNickProperty =
            DependencyProperty.Register("PlayerNick", typeof(string), typeof(AddPlayerUC), new PropertyMetadata(string.Empty));

        public Brush PlayerColor
        {
            get { return (Brush)GetValue(PlayerColorProperty); }
            set { SetValue(PlayerColorProperty, value); }
        }
        public static readonly DependencyProperty PlayerColorProperty =
            DependencyProperty.Register("PlayerColor", typeof(Brush), typeof(AddPlayerUC), new PropertyMetadata(Brushes.Transparent));

        public AddPlayerUC()
        {
            InitializeComponent();
            PawnColor.ItemsSource = colorsList;
        }
    }
}