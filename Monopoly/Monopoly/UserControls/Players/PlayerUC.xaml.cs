﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monopoly.UserControls.Players
{
    /// <summary>
    /// Interaction logic for PlayerUC.xaml
    /// </summary>
    public partial class PlayerUC : UserControl
    {
        public string PlayerNick
        {
            get { return (string)GetValue(PlayerNickProperty); }
            set { SetValue(PlayerNickProperty, value); }
        }
        public static readonly DependencyProperty PlayerNickProperty =
            DependencyProperty.Register("PlayerNick", typeof(string), typeof(PlayerUC), new PropertyMetadata(string.Empty));

        public int PlayerCash
        {
            get { return (int)GetValue(PlayerCashProperty); }
            set { SetValue(PlayerCashProperty, value); }
        }
        public static readonly DependencyProperty PlayerCashProperty =
            DependencyProperty.Register("PlayerCash", typeof(int), typeof(PlayerUC), new PropertyMetadata(0));

        public Brush PlayerColor
        {
            get { return (Brush)GetValue(PlayerColorProperty); }
            set { SetValue(PlayerColorProperty, value); }
        }
        public static readonly DependencyProperty PlayerColorProperty =
            DependencyProperty.Register("PlayerColor", typeof(Brush), typeof(PlayerUC), new PropertyMetadata(null));

        public PlayerUC()
        {
            InitializeComponent();
        }
    }
}
