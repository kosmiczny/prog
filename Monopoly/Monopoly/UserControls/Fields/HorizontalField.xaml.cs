﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monopoly.UserControls.Fields
{
    /// <summary>
    /// Interaction logic for SouthField.xaml
    /// </summary>
    public partial class HorizontalField : UserControl
    {
        public string FieldName
        {
            get { return (string)GetValue(FieldNameProperty); }
            set { SetValue(FieldNameProperty, value); }
        }
        public static readonly DependencyProperty FieldNameProperty =
            DependencyProperty.Register("FieldName", typeof(string), typeof(HorizontalField), new PropertyMetadata(string.Empty));

        public int NameColumn
        {
            get { return (int)GetValue(NameColumnProperty); }
            set { SetValue(NameColumnProperty, value); }
        }
        public static readonly DependencyProperty NameColumnProperty =
            DependencyProperty.Register("NameColumn", typeof(int), typeof(HorizontalField), new PropertyMetadata(0));


        public string FieldPrice
        {
            get { return $"{GetValue(FieldPriceProperty)}$"; }
            set { SetValue(FieldPriceProperty, value); }
        }
        public static readonly DependencyProperty FieldPriceProperty =
            DependencyProperty.Register("FieldPrice", typeof(string), typeof(HorizontalField), new PropertyMetadata(string.Empty));

        public int PriceColumn
        {
            get { return (int)GetValue(PriceColumnProperty); }
            set { SetValue(PriceColumnProperty, value); }
        }
        public static readonly DependencyProperty PriceColumnProperty =
            DependencyProperty.Register("PriceColumn", typeof(int), typeof(HorizontalField), new PropertyMetadata(0));


        public Brush FieldColor
        {
            get { return (Brush)GetValue(FieldColorProperty); }
            set { SetValue(FieldColorProperty, value); }
        }
        public static readonly DependencyProperty FieldColorProperty =
            DependencyProperty.Register("FieldColor", typeof(Brush), typeof(HorizontalField), new PropertyMetadata(null));

        public int ColorColumn
        {
            get { return (int)GetValue(ColorColumnProperty); }
            set { SetValue(ColorColumnProperty, value); }
        }
        public static readonly DependencyProperty ColorColumnProperty =
            DependencyProperty.Register("ColorColumn", typeof(int), typeof(HorizontalField), new PropertyMetadata(0));



        public Brush OwnerColor
        {
            get { return (Brush)GetValue(OwnerColorProperty); }
            set { SetValue(OwnerColorProperty, value); }
        }
        public static readonly DependencyProperty OwnerColorProperty =
            DependencyProperty.Register("OwnerColor", typeof(Brush), typeof(HorizontalField), new PropertyMetadata(Brushes.White));


        public HorizontalField()
        {
            InitializeComponent();
        }
    }
}
