﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Monopoly.UserControls.Fields
{
    /// <summary>
    /// Interaction logic for NorthField.xaml
    /// </summary>
    public partial class VerticalField : UserControl
    {
        public string FieldName
        {
            get { return (string)GetValue(FieldNameProperty); }
            set { SetValue(FieldNameProperty, value); }
        }
        public static readonly DependencyProperty FieldNameProperty =
            DependencyProperty.Register("FieldName", typeof(string), typeof(VerticalField), new PropertyMetadata(string.Empty));

        public int NameRow
        {
            get { return (int)GetValue(NameRowProperty); }
            set { SetValue(NameRowProperty, value); }
        }
        public static readonly DependencyProperty NameRowProperty =
            DependencyProperty.Register("NameRow", typeof(int), typeof(VerticalField), new PropertyMetadata(0));


        public string FieldPrice
        {
            get { return (string)GetValue(FieldPriceProperty); }
            set { SetValue(FieldPriceProperty, value); }
        }
        public static readonly DependencyProperty FieldPriceProperty =
            DependencyProperty.Register("FieldPrice", typeof(string), typeof(VerticalField), new PropertyMetadata(string.Empty));

        public int PriceRow
        {
            get { return (int)GetValue(PriceRowProperty); }
            set { SetValue(PriceRowProperty, value); }
        }
        public static readonly DependencyProperty PriceRowProperty =
            DependencyProperty.Register("PriceRow", typeof(int), typeof(VerticalField), new PropertyMetadata(0));


        public Brush FieldColor
        {
            get { return (Brush)GetValue(FieldColorProperty); }
            set { SetValue(FieldColorProperty, value); }
        }
        public static readonly DependencyProperty FieldColorProperty =
            DependencyProperty.Register("FieldColor", typeof(Brush), typeof(VerticalField), new PropertyMetadata(null));

        public int ColorRow
        {
            get { return (int)GetValue(ColorRowProperty); }
            set { SetValue(ColorRowProperty, value); }
        }
        public static readonly DependencyProperty ColorRowProperty =
            DependencyProperty.Register("ColorRow", typeof(int), typeof(VerticalField), new PropertyMetadata(0));



        public Brush OwnerColor
        {
            get { return (Brush)GetValue(OwnerColorProperty); }
            set { SetValue(OwnerColorProperty, value); }
        }
        public static readonly DependencyProperty OwnerColorProperty =
            DependencyProperty.Register("OwnerColor", typeof(Brush), typeof(VerticalField), new PropertyMetadata(Brushes.White));



        public VerticalField()
        {
            InitializeComponent();
        }
    }
}
