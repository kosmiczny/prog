﻿using System;
using System.Windows.Input;

namespace Monopoly
{
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _action;

        private readonly Predicate<object> _canExecute;

        public DelegateCommand(Action<object> action, Predicate<object> canExecute = null)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke(parameter) ?? true;
        }

        public void Execute(object parameter)
        {
            _action.Invoke(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}