﻿using Model;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Services;
using System.Windows.Input;
using Monopoly.UserControls;
using System.Reflection;

namespace Monopoly.ViewModel
{
    [ImplementPropertyChanged]
    public class PlayersViewModel
    {
        private static IPlayerServices _playerServices;
        public List<PlayerViewModel> players { get; set; } = new List<PlayerViewModel>();
        public bool startButtonEnable { get; set; } = false;

        public PlayersViewModel(IPlayerServices playerServices)
        {
            _playerServices = playerServices;
            players = Enumerable.Range(0, 4).Select(index => new PlayerViewModel(_playerServices.AddPlayer($"Player {index}", Brushes.Transparent))).ToList();
            players.First().AddUCEnable = true;
        }

        public ICommand AddPlayer => new DelegateCommand(x =>
        {
            var newPlayer = players.FirstOrDefault(p => p.isCreated == false);
            newPlayer.isCreated = true;      
            newPlayer.AddUCVisible = false;
            if (players.IndexOf(newPlayer) < 3) players[players.IndexOf(newPlayer) + 1].AddUCEnable = true;
            startButtonEnable = players.Where(player => player.isCreated == true).Count() >= 2 ? true : false;
        });
    }
}