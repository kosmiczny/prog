﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Model;

namespace Monopoly.ViewModel
{
    [ImplementPropertyChanged]
    public class CardViewModel
    {
        public int fieldPosX { get; set; }
        public int fieldPosY { get; set; }
        public Brush color { get; set; }
        public string placeName { get; set; }
        public int buyPrice { get; set; }
        public Brush owner { get; set; }
        public int[] costs { get; set; }
        public int mortgagePledge { get; set; }
        public bool pledged { get; set; }

        public CardViewModel(Card card)
        {
            fieldPosX = card.fieldPosX;
            fieldPosY = card.fieldPosY;
            color = card.color;
            placeName = card.placeName;
            buyPrice = card.buyPrice;
            owner = card.owner;
            costs = card.costs;
            mortgagePledge = card.mortgagePledge;
        }
    }
}
