﻿using Model;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Monopoly.ViewModel
{
    [ImplementPropertyChanged]
    public class PlayerViewModel
    {
        public int id { get; set; }
        public string nick { get; set; }
        public int cash { get; set; }
        public PawnViewModel pawn { get; set; }
        public bool AddUCEnable { get; set; } = false;
        public bool AddUCVisible { get; set; } = true;
        public bool InfoUCVisible { get; set; } = true;
        public bool isCreated { get; set; } = false;
        public int queue { get; set; } = 0;
        public PlayerViewModel(Player player)
        {
            id = player.id;
            nick = player.nick;
            cash = player.cash;
            pawn = new PawnViewModel()
            {
                pawn = player.pawn.pawn,
                posX = player.pawn.posX,
                posY = player.pawn.posY
            };
        }
    }

    [ImplementPropertyChanged]
    public class PawnViewModel
    {
        public int posX { get; set; }
        public int posY { get; set; }
        public Rectangle pawn { get; set; }
    }
}
