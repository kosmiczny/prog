﻿using Model;
using PropertyChanged;
using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly.ViewModel
{
    [ImplementPropertyChanged]
    public class CardsViewModel
    {
        private static ICardServices _cardServices;
        public List<CardViewModel> cards { get; set; } = new List<CardViewModel>();

        public CardsViewModel(ICardServices cardServices)
        {
            _cardServices = cardServices;
            GetCards();
        }

        public void GetCards()
        {
            var cards = _cardServices.GetCards();
            foreach (var card in cards)
                this.cards.Add(new CardViewModel(card));
        }

        public CardViewModel TakeCard(int posX, int posY)
        {
            return cards.Where(card => card.fieldPosX == posX && card.fieldPosY == posY).FirstOrDefault();
        }
    }
}
