﻿using Model;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Linq;
using Services;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.IO;

namespace Monopoly.ViewModel
{
    [ImplementPropertyChanged]
    public class GameViewModel
    {
        private static IGameServices _gameServices = new GameServices();
        private static IPlayerServices _playerServices = new PlayerServices();
        private static ICardServices _cardServices = new CardServices();
        public PlayersViewModel playersViewModel { get; set; }
        public CardsViewModel cardsViewModel { get; set; }
        public CardViewModel currentCard { get; set; }
        public ChanceCardViewModel chanceCard { get; set; } = new ChanceCardViewModel();
        public ImageSource firstDice { get; set; }
        public ImageSource secondDice { get; set; }
        #region EnableVisible
        public bool rollDiceEnable { get; set; } = false;
        public bool startButtonVisible { get; set; } = true;
        public bool endTurnEnable { get; set; } = false;
        public bool cardDisplayVisible { get; set; } = false;
        public bool pledgeCardDisplayVisible { get; set; } = false;
        public bool chanceCardVisible { get; set; } = false;
        public string turnInfo { get; set; }
        private static int doublet { get; set; }
        #endregion
        public Brush turn { get; set; }

        public GameViewModel()
        {
            playersViewModel = new PlayersViewModel(_playerServices);
            cardsViewModel = new CardsViewModel(_cardServices);
            ShowDices(1, 1);
        }

        public ICommand StartGame => new DelegateCommand(x =>
        {
            playersViewModel.players.Where(p => p.isCreated == false).ToList().ForEach(player => { player.InfoUCVisible = false; player.AddUCVisible = false; });
            playersViewModel.players.RemoveAll(p => p.isCreated == false);
            turn = playersViewModel.players.First().pawn.pawn.Fill;
            startButtonVisible = false;
            rollDiceEnable = true;
        });

        public ICommand RollDices => new DelegateCommand(x =>
        {
            var rolledNumbers = _gameServices.RollDices();
            ShowDices(rolledNumbers[0], rolledNumbers[1]);
            if (rolledNumbers[0] == rolledNumbers[1]) doublet = rolledNumbers[0] + rolledNumbers[1];
            MovePawn(TakePlayerWithTurn().pawn, rolledNumbers[0] + rolledNumbers[1]);
            ShowCard();
            TakePlayerWithTurn().cash -= Pay();
            rollDiceEnable = false;
            endTurnEnable = true;
        });

        public ICommand NextTurn => new DelegateCommand(x =>
        {
            do
            {
                SwitchTurn();
                TakePlayerWithTurn().queue -= TakePlayerWithTurn().queue > 0 ? 1 : 0;
            } while (TakePlayerWithTurn().queue != 0);
            rollDiceEnable = true;
            endTurnEnable = false;
            cardDisplayVisible = false;
            pledgeCardDisplayVisible = false;
            chanceCardVisible = false;
        });

        private void SwitchTurn()
        {
            var currentPlayerIndex = playersViewModel.players.IndexOf(TakePlayerWithTurn());
            turn = currentPlayerIndex != playersViewModel.players.Count - 1 ? playersViewModel.players[currentPlayerIndex + 1].pawn.pawn.Fill : playersViewModel.players.First().pawn.pawn.Fill;
        }

        private int Pay()
        {
            var player = TakePlayerWithTurn();
            var pawn = player.pawn;
            var place = cardsViewModel.TakeCard(pawn.posX, pawn.posY);
            
            if (place != null && place?.owner != Brushes.White && place?.owner != player.pawn.pawn.Fill)
            {
                playersViewModel.players.Where(p => p.pawn.pawn.Fill == place.owner).FirstOrDefault().cash += place.costs[0];
                return place.costs[0];
            }
            return 0;
        }

        public void ShowCard()
        {
            var currentPawn = TakePlayerWithTurn().pawn;
            currentCard = cardsViewModel.TakeCard(currentPawn.posX, currentPawn.posY);
            if (currentCard != null)
            {
                if (currentCard.pledged == false) cardDisplayVisible = true; else pledgeCardDisplayVisible = true;
            }
        }

        public ICommand BuyPlace => new DelegateCommand(x =>
        {
            var player = TakePlayerWithTurn();
            var pawn = player.pawn;
            var card = cardsViewModel.TakeCard(pawn.posX, pawn.posY);
            if (card.owner == Brushes.White && card.buyPrice <= player.cash)
            {
                player.cash -= card.buyPrice;
                card.owner = player.pawn.pawn.Fill;
            }
        });

        public void ShowDices(int firstDice, int secondDice)
        {
            this.firstDice = new BitmapImage(new Uri($"images/dice/{firstDice}.png", UriKind.Relative));
            this.secondDice = new BitmapImage(new Uri($"images/dice/{secondDice}.png", UriKind.Relative));
        }

        public PlayerViewModel TakePlayerWithTurn()
        {
            return playersViewModel.players.Where(p => p.pawn.pawn.Fill.ToString() == turn.ToString()).First();
        }

        public void MovePawn(PawnViewModel pawn, int steps)
        {
            string turnInfo = null;
            var result = _playerServices.MovePawn(pawn.posX, pawn.posY, steps, ref turnInfo);
            this.turnInfo = turnInfo;
            pawn.posX = result[0];
            pawn.posY = result[1];
            TakePlayerWithTurn().cash += result[2];
            TakePlayerWithTurn().queue += result[3];
            if(result[4] > 0)
            {
                if(result[4] >= 1 && result[4] <= 14)
                {
                    var chanceCard = _playerServices.blueChanceCards.FirstOrDefault(card => int.Parse(card.Split(' ')[0]) == result[4]).Split(' ');
                    this.chanceCard.text = string.Join(" ", chanceCard.ElementAt(1).Split('_'));
                    this.chanceCard.color = Application.Current.FindResource("BlueCard");
                    chanceCardVisible = true;
                }
                else if(result[4] >= 15 && result[4] <=24)
                {
                    var chanceCard = _playerServices.redChanceCards.FirstOrDefault(card => int.Parse(card.Split(' ')[0]) == result[4]).Split(' ');
                    this.chanceCard.text = string.Join(" ", chanceCard.ElementAt(1).Split('_'));
                    this.chanceCard.color = Application.Current.FindResource("RedCard");
                    chanceCardVisible = true;
                }
            }
        }
    }
}
